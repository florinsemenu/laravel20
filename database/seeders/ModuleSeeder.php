<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Module;
use DB;

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //con eloquent
        Module::create([
            'course' => '2nd',
            'name' => 'Desarrollo Web en entorno servidor',
            'code' => '0613',
            'short_name' => 'Servidor',
            'abreviation' => 'DWS'
        ]);
    }
}
